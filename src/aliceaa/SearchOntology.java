package aliceaa;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.jxta.peergroup.PeerGroup;
import net.jxta.share.ContentAdvertisement;
import net.jxta.share.client.CachedListContentRequest;
import net.jxta.share.client.GetContentRequest;

public class SearchOntology {
    
    public PeerGroup ontoPeerGroup=null;
    private String searchValue =null, Path = null;
    private long RequestTime;
    protected ListRequestor requestor =null;
    public static ContentAdvertisement [] contents=null;
    
    public SearchOntology(PeerGroup group,String searchKey, long ontoRequestTime) {
        this.ontoPeerGroup = group;
        this.searchValue = searchKey; 
        this.RequestTime = ontoRequestTime;
    }
    
    public void run() { 
        if (Tools.getOS().equalsIgnoreCase("linux")) {
                Path = "//ontologyFiles//";
            } else 
            Path = "\\ontologyFiles\\";
        requestor = new ListRequestor(ontoPeerGroup,searchValue, Path, RequestTime);        
        requestor.activateRequest();
     }        

    public ContentAdvertisement [] getContentAdvs() {
        return requestor.searchResult;
    } 
}   //SearchOntology

class GetFile extends GetContentRequest implements ContentListener {
    
    private ContentAdvertisement searchR = null;
    private String url = null;
    private ContentListener listener;
    private long ontoGetTime, originTime;

    public GetFile(PeerGroup group, ContentAdvertisement contentAdv, File destination, ContentListener cl, long time) {
        super(group, contentAdv, destination);
        searchR = contentAdv;
        url = destination.getAbsolutePath();
        this.listener = cl;
        this.originTime = time;
    }
    
    public ContentAdvertisement getContentAdvertisement() {
        return searchR;
    }
    
    @Override
    public void notifyUpdate(int percentage) {
     //   System.out.println("[-]Downloading in Progress!!!!!\n"+percentage);
    }
    
    @Override
    public void notifyDone() {
        ontoGetTime = System.currentTimeMillis();
        System.out.println("Total time to download the ontology file is: " +(ontoGetTime - originTime)+" milliseconds\n");
        try { 
            new GenerateRDF();
        } catch (IOException ex) {
            Logger.getLogger(GetFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void notifyFailure() {
        System.out.println("[-]Downloading File is Failed!!!!!\n");
    }

    @Override
    public void finishedRet(String url) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}   //GetFile

class ListRequestor extends CachedListContentRequest {
    
    boolean gotOne = false;
    public static ContentAdvertisement [] searchResult = null;
    private String Path = null;
    private File filePath = new File(System.getProperty("user.dir"));
    private PeerGroup grp = null;
    private long requestTime;
    
    public ListRequestor(PeerGroup pg , String SubStr, String p, long aaTime){
        super(pg,SubStr);
        this.grp = pg;
        this.Path = p;
        this.requestTime = aaTime;
    }

    @Override
    public void notifyMoreResults() {
        ContentAdvertisement [] r = this.getResults();
        if(r != null) {
            for (int i=0; i<r.length; i++) {
                ContentAdvertisement myCAd = r[i];                
                if(!gotOne) {
                    File tmpFile = new File (filePath+Path+myCAd.getName());
                    ContentListener ml = new ContentListener() {
                        public void finishedRet(String str) {
                           // System.out.println("File Downloaded!\n");
                        }   //finishedRet
                    };  //ContentListener interface                
                    try {
                        GetFile gf = new GetFile(grp, r[i], tmpFile, ml, requestTime);
                    } catch (Exception e ) {
                        e.printStackTrace();
                        }
                    gotOne = true;
                } // if !gotOne
            }   // for int i=0
        }   //if r!=null
        else {
            System.out.println("No results");
        }      
    }
    
    public ContentAdvertisement [] getContentAdvs() { 
       // System.out.println("DOWN: " +searchResult.toString());
        return searchResult;
    }    
}   //ListRequestor