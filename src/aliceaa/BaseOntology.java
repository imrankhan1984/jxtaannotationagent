package aliceaa;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.util.FileManager;

public class BaseOntology extends Object {
        
    private static String Path = "\\ontologyFiles\\TemperatureOntology.owl";
    
    // URI for vocabulary elements
    protected static final String uri = "http://BaseOntology.owl#";
    protected static final String ssn = "http://purl.oclc.org/NET/ssnx/ssn#";
             
    // Define the property labels and objects   
    public static Property hasUnit = null;       
    public static Property observedBy = null;       
    public static Property observes = null;       
    public static DatatypeProperty hasSensingTime = null;
    public static DatatypeProperty hasSensorName = null;
    public static DatatypeProperty hasSensingDate = null;
    public static DatatypeProperty hasLatitude = null;
    public static DatatypeProperty hasLongitude = null;
    public static DatatypeProperty hasValue  = null;
    
    // Define the Class labels 
    public static OntClass  SensorLocation = null;       
    public static OntClass  Latitude = null;       
    public static OntClass  Longitude = null;       
    public static OntClass  MeasurementUnit = null;
    public static OntClass  DegreeCelsius = null;
    public static OntClass  DegreeFahrenheit = null;
    public static OntClass  ObservedProperty = null;
    public static OntClass  Temperature = null;
    public static OntClass  Sensor  = null;   
    public static OntClass  TemperatureSensor  = null;   
    public static OntClass  SensorOutput  = null;   
    public static OntClass  TemperatureOutput  = null;
    

    static {
        try {
            if (Tools.getOS().equalsIgnoreCase("linux")) {
                Path = "//ontologyFiles//TemperatureOntology.owl";
            }
            OntModel model = ModelFactory.createOntologyModel();
            FileManager.get().readModel( model, System.getProperty("user.dir")+Path);
                 
            // Instantiate the properties          
            hasUnit = model.getProperty(uri,"hasUnit"); 
            observedBy = model.getProperty(ssn,"observedBy");
            observes = model.getProperty(ssn,"observes");
            hasSensingTime = model.getDatatypeProperty(uri+"hasSensingTime");
            hasSensingDate = model.getDatatypeProperty(uri+"hasSensingDate");
            hasLatitude = model.getDatatypeProperty(uri+"hasLatitude");
            hasLongitude = model.getDatatypeProperty(uri+"hasLongitude");
            hasValue = model.getDatatypeProperty(ssn+"hasValue");
            hasSensorName     =  model.getDatatypeProperty(uri+"hasSensorName");

            // Instantiate the Class
            SensorLocation = model.getOntClass(uri+"SensorLocation");
            Latitude = model.getOntClass(uri+"Latitude");
            Longitude = model.getOntClass(uri+"Longitude");
            MeasurementUnit = model.getOntClass(uri+"MeasurementUnit");
            DegreeCelsius = model.getOntClass(uri+"DegreeCelsius");
            DegreeFahrenheit = model.getOntClass(uri+"DegreeFahrenheit");
            ObservedProperty = model.getOntClass(uri+"ObservedProperty");
            Temperature = model.getOntClass(uri+"Temperature");
            Sensor = model.getOntClass(ssn+"Sensor");
            TemperatureSensor = model.getOntClass(uri+"TemperatureSensor");
            SensorOutput = model.getOntClass(ssn+"SensorOutput");
            TemperatureOutput = model.getOntClass(uri+"TemperatureOutput");

            // Adding SubClass	 
            SensorLocation.addSubClass(Latitude);
            SensorLocation.addSubClass(Longitude);
            MeasurementUnit.addSubClass(DegreeCelsius);
            MeasurementUnit.addSubClass(DegreeFahrenheit);
            ObservedProperty.addSubClass(Temperature);
            SensorOutput.addSubClass(TemperatureOutput);
            Sensor.addSubClass(TemperatureSensor);
            
        } catch (Exception e){
            System.out.println("Failed: " + e);         
    }
    }
}
