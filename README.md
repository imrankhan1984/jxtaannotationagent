# README #

Hi, the code for Annotation Agent is stored here

### What is this repository for? ###

The Annotation Agent (AA) receives data from the SunSpot sensors sent to it through Semantic Service which is running on the same machine as AA. It is important to have Semantic Service up and running before you receive SunSpot data. It's code can be found at https://bitbucket.org/imrankhan1984/semanticservice

### How do I get set up? ###

All necessary files are there for you to start using the AA code, including the Jenna, JXTA and JXTA-CMS jar files. You will need a working NetBeans IDE. The nbproject folder of the repository contains all the required files.

For SemanticService you will need a working Tomcat webserver. 

You will also need a running git environment to checkout the code from all the repositories.

### Contribution guidelines ###

If you are making changes to the code then please don't forget to **push** the updated code. 

### Who do I talk to? ###

contact: imrankhan1984@gmail.com