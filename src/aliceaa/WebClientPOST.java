package aliceaa;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WebClientPOST {
    
    public void PostModel(DataClass d) throws IOException {   
        
        long finalPostTime = System.currentTimeMillis();
        
        HttpURLConnection connection = null;
        String targetURL="http://semanticfireapp.appspot.com/SensorReading";
        DataOutputStream wr = null;
        OutputStreamWriter oos = null;
        try { 
            URL url= new URL(targetURL);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");   
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "text/xml");
            oos = new OutputStreamWriter (connection.getOutputStream());
            d.writeInStream(oos);
            oos.close();
            
            // Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }     rd.close();
            System.out.println("Reply from Google Apps received after: " +(System.currentTimeMillis() - finalPostTime)+ " milliseconds");
        } catch (IOException ex) {
            Logger.getLogger(WebClientPOST.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                oos.close();
            }
    }//PostModel(DataClass d)
}   //WebClientPOST
