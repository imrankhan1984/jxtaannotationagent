package aliceaa;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;
import java.io.IOException;
//import convertOwl.BaseOnto;

public class GenerateRDF implements java.io.Serializable {
    
    WebClientPOST client=new WebClientPOST();
    
    boolean posted = false;
        
    public GenerateRDF() throws IOException {
        
        long annotationStartTime = System.currentTimeMillis();
        
        String longitude = Tools.getLon();
        String unit = Tools.getUnit();
        String sender = Tools.getHardwareID();  // to get the last 4 digits of SunSpots
        String latitude = Tools.getLat();
		String n = Tools.getSenderName();
        double sensorMeasurement = Tools.getMeasrurement();
        long sensingTime = Tools.getSentTime();

        OntModel model = ModelFactory.createOntologyModel();

        model.setNsPrefix("ssn",BaseOntology.ssn);
        model.setNsPrefix("base",BaseOntology.uri);
        
        Literal dateTime = model.createTypedLiteral(Tools.convertTime(sensingTime), XSDDatatype.XSDdateTime);
        Literal date = model.createTypedLiteral(Tools.convertDate(sensingTime), XSDDatatype.XSDdate);
        
        Resource SunSpot_1 = model.createResource(BaseOntology.uri+"_"+sender)
            .addProperty(RDF.type ,BaseOntology.TemperatureSensor)
            .addProperty(BaseOntology.observes, BaseOntology.Temperature)
            .addProperty(BaseOntology.hasLatitude, latitude)
            .addProperty(BaseOntology.hasLongitude, longitude)
            .addProperty(BaseOntology.hasSensorName, n+"_"+sender);

        Resource SunSpotOutput_1 = model.createResource(BaseOntology.uri+sender+"_Output") 
            .addProperty(RDF.type ,BaseOntology.TemperatureOutput)
            .addProperty(BaseOntology.observedBy, SunSpot_1)
            .addLiteral(BaseOntology.hasValue, sensorMeasurement)
            .addProperty(BaseOntology.hasSensingTime, dateTime)
            .addProperty(BaseOntology.hasSensingDate, date);
   
        if (unit.equalsIgnoreCase("C")){
            SunSpotOutput_1.addProperty(BaseOntology.hasUnit,BaseOntology.DegreeCelsius);
        } else 
            SunSpotOutput_1.addProperty(BaseOntology.hasUnit,BaseOntology.DegreeFahrenheit);
        
        long annotationEndTime = System.currentTimeMillis();
        System.out.println("Total annotation time is: " +(annotationEndTime - annotationStartTime)+ " milliseconds");
    
        DataClass obj = new DataClass(model);
        model.write(System.out);
        client.PostModel(obj);
        posted = true;    
    }  
}