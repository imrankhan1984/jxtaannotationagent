package aliceaa;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.jxta.discovery.DiscoveryEvent;
import net.jxta.discovery.DiscoveryListener;
import net.jxta.discovery.DiscoveryService;
import net.jxta.exception.PeerGroupException;
import net.jxta.id.IDFactory;
import net.jxta.peer.PeerID;
import net.jxta.peergroup.PeerGroup;
import net.jxta.peergroup.PeerGroupID;
import net.jxta.pipe.OutputPipeEvent;
import net.jxta.pipe.OutputPipeListener;
import net.jxta.pipe.PipeMsgEvent;
import net.jxta.pipe.PipeMsgListener;
import net.jxta.pipe.PipeService;
import net.jxta.platform.NetworkConfigurator;
import net.jxta.platform.NetworkManager;
import net.jxta.protocol.ModuleImplAdvertisement;
import net.jxta.protocol.PeerGroupAdvertisement;
import net.jxta.protocol.PipeAdvertisement;

public class AliceAA implements DiscoveryListener, PipeMsgListener, OutputPipeListener {
    
    private static final String serviceName = "Ontology_Service";
    private final static int TIMEOUT = 3*1000;
    private static PeerGroup netPeerGroup, ontoPeerGroup;;
    private PipeService ontoPipe;
    private DiscoveryService discovery, ontoDiscoService;;  
    private final PipeAdvertisement pipeAdv = null;    
    private int messageNumber = 0;   
    boolean noticeFrmA = false;
    boolean noticeFrmB = false;
    boolean noticeFrmC = false;
    boolean noticeFrmJ = false;
    boolean sending = false;
    
    private static File Path = null;
    static SearchOntology fs;
    private final static String temperatureOntology = "TemperatureOntology.owl";
    private final static String lightOntology = "LightOntology.owl";
    private final static String humidityOntology = "HumidityOntology.owl";
    private final static String carbonOntology = "CarbonOntology.owl";
    private final static String baseOntology = "BaseOntology.owl";
  
    PeerGroupAdvertisement ontoGrpAdv =null;
    Set<PeerID> peerSet = new HashSet<>(); 
    
    final private String peerName;
    private final PeerID peerID;
    private final File conf;
    private NetworkManager manager;    
    
    private final String ontoPeerGrpID = "urn:jxta:uuid-4E0742B0E54F4D0ABAC6809BB82A311E02";
    public final static String aaID = "urn:jxta:uuid-59616261646162614E50472050325033426F624A78744150A565F203";

    boolean post = false;

    int serverPort;   
    
    URI uri = new URI("urn:jxta:uuid-4E0742B0E54F4D0ABAC6809BB82A311ED5510BE8AE1B4B4CBDA3C969FED126A307");

    public boolean isRcvd;
    String clientString;
    String peerString;
    long rcvdTime;
    Map map = new HashMap();

    public static void main(String[] args) throws IOException, URISyntaxException, PeerGroupException {
        if (Tools.getOS().equalsIgnoreCase("linux")) {
            Path = new File (System.getProperty("user.dir")+"//ontologyFiles");
        } else {
            Path = new File (System.getProperty("user.dir")+"\\ontologyFiles");
        }
        System.out.println("The os is: "+Tools.getOS());
        Logger.getLogger("net.jxta").setLevel(Level.OFF);
        deleteExistingFiles(Path);
        long start = System.currentTimeMillis();        
        AliceAA aliceJP = new AliceAA (9730);
        aliceJP.startAA(); 
        aliceJP.SensorDataReader();
        long end = System.currentTimeMillis();
        System.out.println("The OCD is:" +(end-start));                
    } // main

    public AliceAA (int port) throws IOException, URISyntaxException {
        peerName  = "Annotation_Agent";
        peerID = (PeerID)IDFactory.fromURI(new URI(aaID));
        conf = new File("." + System.getProperty("file.separator") + peerName);
        
        NetworkManager.RecursiveDelete(conf);
        
        try {
            manager = new NetworkManager(NetworkManager.ConfigMode.EDGE,peerName, conf.toURI());
        }
        catch (IOException e) {
        }
        NetworkConfigurator configurator = manager.getConfigurator();
        configurator.clearRendezvousSeeds();
        configurator.setTcpPort(port);
        configurator.setTcpEnabled(true);
        configurator.setTcpIncoming(true);
        configurator.setTcpOutgoing(true);
        configurator.setUseMulticast(true);
        configurator.setPeerID(peerID);
        
        try {
            netPeerGroup = manager.startNetwork();
        }catch(PeerGroupException e) {
            
        }             
    }   //AnnotationAgent

    public void startAA() throws PeerGroupException, IOException {
        try {            
            getService();
            searchForGroup();    
        } catch (Exception e) {
          }        
    } //startCAN
    
    private void getService() {
        discovery = netPeerGroup.getDiscoveryService();
        discovery.addDiscoveryListener(this);
    }

private void searchForGroup() throws InterruptedException {
        
    Enumeration adv=null;       
        int count =0;      
        while(count < 3){
            try {                
                adv = discovery.getLocalAdvertisements(DiscoveryService.GROUP,"Name","OntoPeerGroup");
                if((adv != null) && adv.hasMoreElements()){
                    
                    ontoGrpAdv = (PeerGroupAdvertisement)adv.nextElement();
                    ontoPeerGroup = netPeerGroup.newGroup(ontoGrpAdv);
                    joinToGroup(ontoPeerGroup);
                    System.out.println("\nJoined Group:\n"+ontoPeerGroup+"\nwith Adv\n"+ontoGrpAdv);
                    break;
                }else{
                    
                    discovery.getRemoteAdvertisements(null,DiscoveryService.GROUP,"Name","OntoPeerGroup",1);
                }
                Thread.sleep(TIMEOUT);
            } catch (IOException ex) {
                ex.printStackTrace();
            }catch(PeerGroupException e){
            System.out.println("[-]Fatal Error:" + e.getMessage());
            e.printStackTrace();
            System.exit(-1);
            }
            count++;
        }        
    }   //searchForGroup
    
    private PeerGroup createGroup() { 
        
        PeerGroup myNewGroup = null;
        try{
            ModuleImplAdvertisement myMIA = netPeerGroup.getAllPurposePeerGroupImplAdvertisement();
            myNewGroup = netPeerGroup.newGroup(getGID(),
                                               myMIA,
                                               "OntoPeerGroup",
                                               "Ontology Sharing");
            ontoGrpAdv = myNewGroup.getPeerGroupAdvertisement();
            discovery.publish(ontoGrpAdv);
            discovery.remotePublish(ontoGrpAdv);            
        }catch(Exception e){
            System.out.println("[*]Fatal Error:" + e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
        return myNewGroup;
    }   //createGroup
    
    private PeerGroupID getGID() throws Exception {
        return (PeerGroupID) IDFactory.fromURI(new URI(ontoPeerGrpID));
    }    

    private void joinToGroup(PeerGroup group){ 
        try{
                ontoDiscoService = group.getDiscoveryService();
                ontoDiscoService.publish(group.getPeerAdvertisement());
                ontoDiscoService.remotePublish(group.getPeerAdvertisement());            
        }catch(Exception e){
                System.out.println("[!]Fatal Error: " + e.getMessage());
                e.printStackTrace();
                System.exit(-1);
            }
    }   //joinToGroup
    
    public void pipeMsgEvent(PipeMsgEvent pme) {
        long rcvdTime = System.currentTimeMillis();  
    }

    public void outputPipeEvent(OutputPipeEvent ope) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    public static PeerGroup getOntoPeerGroup() {
     return ontoPeerGroup;   
    }
    public PeerGroupAdvertisement getOntoGrpAdv(){
        return ontoGrpAdv;
    }
    
/*
Now that we have the Key Value (K,V)pairs in a String array
we can utilize them by map.get(key) mechanism.
The keys are according SenML except sv and u since Java Map class 
needs unique K,V relationship. A key cannot have mutliple values against it.
So here are keys from the SenML string we get from the sensor that can
be used to get the values.

Key      Value                                       Type
===      =====                                       ====
n        sensor that send the data                   String  
e.g. temperature_sensor or light_sensor
v        sensor measurement e.g. 24.0                double
sve      longitude value                             String
ue       unit (i.e. lon)                             String
svn      latitude value                              String
un       unit (i.e. lat)                             String
bn       sensorID e.g. IP address                    String
or IEEE hardware address
bt       time when the sensor sent the data          long
bu       unit of the measurement                     String

Examples
to get & store a value use Tools.setUnit(map.get(bu));
to use a value in your code usr char unit = Tools.getUnit();
*/
    
    private void SensorDataReader() throws IOException {                   
        new Thread("Sensor Data Reader thread") {    
            @Override
            public void run() {
                try {
                    ServerSocket sSocket = new ServerSocket(7789);                           
                    while (true) {
                        Socket conn = sSocket.accept();
                        BufferedReader inFromClient = 
                        new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        long aaReachTime = System.currentTimeMillis();
                        clientString = Tools.fromJSON(inFromClient.readLine());  
                        String[] kvPair = clientString.split(" ");
                        for(String kvPairs: kvPair) {
                            String[] kv = kvPairs.split(":");
                            map.put(kv[0], kv[1]);
                        }
                        Tools.setSenderName((String) map.get("n"));
                        Tools.setSentTime(new Long (map.get("bt").toString()));
                        Tools.setMeasrurement(new Double (map.get("v").toString()));
                        Tools.setLocation((String)map.get("sve"), (String)map.get("svn"));
                        Tools.setLonUnit((String) map.get("ue"));
                        Tools.setLatUnit((String) map.get("un"));
                        Tools.setHardwareID((String) map.get("bn"));                        
                        Tools.setUnit((String)map.get("bu"));  
                        System.out.println("The time taken by JSON String "+ (++messageNumber)+" from Sensor to AA is: "
                                +(aaReachTime - Tools.getSentTime()) + " milliseconds\n");
                        //if(Path)
                        if (Path.list().length > 0) {
                            new GenerateRDF();                             
                        } else {
                            long ontoRequestTime = System.currentTimeMillis();
                            String myUnit = Tools.getUnit();
                            if(Tools.getUnit().equalsIgnoreCase("C")||Tools.getUnit().equalsIgnoreCase("F")) {
                                fs = new SearchOntology(getOntoPeerGroup(), temperatureOntology, ontoRequestTime);
                            } else if (Tools.getUnit().equalsIgnoreCase("lux")){
                                fs = new SearchOntology(getOntoPeerGroup(), temperatureOntology, ontoRequestTime);
                            }
                            
                            fs.run();
                        }                               
                        post = true;
                    }    // while             
                } //try
                 catch (IOException ex) {
                    Logger.getLogger(AliceAA.class.getName()).log(Level.SEVERE, null, ex);
                 } // catch
            }//run
        }.start(); //thread      
    }    //sensor data reader

    @Override
    public void discoveryEvent(DiscoveryEvent de) {        
    }
    
    public static void deleteExistingFiles(File f2d) {
        File file = f2d;
        String[] ontoFiles;
        if(file.isDirectory()) {
            ontoFiles = file.list();
            for(int i=0; i<ontoFiles.length; i++) {
                File files2d = new File(file, ontoFiles[i]);
                files2d.delete();
            }   // for
        }   //if
    }   // deleteExistingOntoFiles
    
}   //Annotation Agent