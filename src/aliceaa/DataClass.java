package aliceaa;

import com.hp.hpl.jena.rdf.model.Model;
import java.io.OutputStreamWriter;

public class DataClass implements java.io.Serializable {

    private Model fModel;
    
    public DataClass(Model fModel) {
        this.fModel = fModel;
    }
    public void writeInStream(OutputStreamWriter oos) {
        fModel.write(oos);
    }
}
