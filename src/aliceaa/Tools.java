/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package aliceaa;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import javax.swing.JOptionPane;
import net.jxta.credential.AuthenticationCredential;
import net.jxta.credential.Credential;
import net.jxta.document.Attributable;
import net.jxta.document.Attribute;
import net.jxta.document.Element;
import net.jxta.document.MimeMediaType;
import net.jxta.document.StructuredDocument;
import net.jxta.document.StructuredTextDocument;
import net.jxta.endpoint.Message;
import net.jxta.endpoint.Message.ElementIterator;
import net.jxta.endpoint.MessageElement;
import net.jxta.membership.MembershipService;
import net.jxta.peer.PeerID;
import net.jxta.peergroup.PeerGroup;
import net.jxta.platform.NetworkConfigurator;
import net.jxta.platform.NetworkManager;
import net.jxta.rendezvous.RendezVousService;

public class Tools {

    public static String senderName = "";
    public static long sentTime;
    public static double measurement;
    public static String unit;
    public static String hardwareID = "";
    public static String lon;
    public static String lat;
    public static String lonUnit = "";
    public static String latUnit = "";
    
    public Tools() {
    }
    // Set sensor name that sent the data, e.g. temp_sensor/light_sensor          
    public static void setSenderName(String sn) {
        senderName = sn;
    }
    // Get sensor name that sent the data, e.g. temp_sensor/light_sensor
    public static String getSenderName(){
        return senderName;
    }
    // Set time when sensor sent the data
    public static void setSentTime(long st) {
        sentTime = st;
    }
    // Get time when sensor sent the data
    public static long getSentTime(){
        return sentTime;
    }    
    // Set sensor measurement value    
    public static void setMeasrurement(double d) {
        measurement = d;
    }      
    // Get sensor measurement value  
    public static double getMeasrurement(){
        return measurement;
    }   
    // Set the measurement Unit      
    public static void setUnit(String u) {
        unit = u;
    }      
    // Get the measurement Unit 
    public static String getUnit(){
        return unit;
    }   
    // Set the sensor ID, e.g. IP address/IEEE hardware address 
    public static void setHardwareID(String id) {
        hardwareID = id;
    }      
    // Get the sensor ID, e.g. IP address/IEEE hardware address 
    public static String getHardwareID(){
        String last4Digits = hardwareID.substring(hardwareID.lastIndexOf("."));
        last4Digits = last4Digits.replaceFirst(".","");
        return last4Digits;
    }
    // Set the sensor location in string. 
    public static void setLocation(String a, String b) {
        lon = a; // logitude value
        lat = b; // latitude value
    }      
    // Get the sensor longitude location
    public static String getLon(){
        return lon; 
    }
    // Get the sensor lotitude location
    public static String getLat(){
        return lat; 
    }
        // Get the sensor longitude location
    public static void setLonUnit(String ue){
        lonUnit = ue; 
    }
    // Get the sensor lotitude location
    public static String getLonUnit(){
        return lonUnit; 
    }
            // Get the sensor longitude location
    public static void setLatUnit(String un){
        latUnit = un; 
    }
    // Get the sensor lotitude location
    public static String getLatUnit(){
        return latUnit; 
    }
    
    
    public static void popConnectedRendezvous(RendezVousService TheRendezVous, String Name) {        
        List<PeerID> TheList = TheRendezVous.getLocalRendezVousView();
        Iterator<PeerID> Iter = TheList.iterator();
        int Count = 0;     
        while (Iter.hasNext()) {          
            Count = Count + 1;
            PopInformationMessage(Name, "Connected to rendezvous:\n\n"+ Iter.next().toString());           
        }
        if (Count==0) {           
            PopInformationMessage(Name, "No rendezvous connected to this rendezvous!");            
        }
    }   //popConnectedRendezvous
    
    public static void popConnectedPeers(RendezVousService TheRendezVous, String Name) {        
        List<PeerID> TheList = TheRendezVous.getLocalRendezVousView();
        Iterator<PeerID> Iter = TheList.iterator();
        int Count = 0;        
        while (Iter.hasNext()) {            
            Count = Count + 1;            
            PopInformationMessage(Name, "Peer connected to this rendezvous:\n\n"+ Iter.next().toString());            
        }        
        if (Count==0) {            
            PopInformationMessage(Name, "No peers connected to this rendezvous!");            
        }        
    }   //popConnectedPeers
    
    public static void CheckForMulticastUsage(String Name, NetworkConfigurator TheNC) throws IOException {        
        if (JOptionPane.YES_OPTION==PopYesNoQuestion(Name, "Do you want to enable multicasting?")) {
            TheNC.setUseMulticast(true);            
        } else {            
            TheNC.setUseMulticast(false);            
        }        
    }   //CheckForMulticastUsage
    
    public static void CheckForRendezVousSeedAddition(String Name, String TheSeed, NetworkConfigurator TheNC) {      
        if (JOptionPane.YES_OPTION==PopYesNoQuestion(Name, "Do you want to add seed: " + TheSeed + "?")) {
            URI LocalSeedingRendezVousURI = URI.create(TheSeed);
            TheNC.addSeedRendezvous(LocalSeedingRendezVousURI);           
        }
    }   //CheckForRendezVousSeedAddition
    
    public static void PopInformationMessage(String Name, String Message) {        
        JOptionPane.showMessageDialog(null, Message, Name, JOptionPane.INFORMATION_MESSAGE);        
    }   //PopInformationMessage
    
    public static void PopErrorMessage(String Name,String Message) {        
        JOptionPane.showMessageDialog(null, Message, Name, JOptionPane.ERROR_MESSAGE);        
    }   //PopErrorMessage
    
    public static void PopWarningMessage(String Name, String Message) {        
        JOptionPane.showMessageDialog(null, Message, Name, JOptionPane.WARNING_MESSAGE);       
    }   //PopWarningMessage
    
    public static int PopYesNoQuestion(String Name, String Question) {        
        return JOptionPane.showConfirmDialog(null, Question, Name, JOptionPane.YES_NO_OPTION);        
    }   //PopYesNoQuestion
    
    public static void CheckForExistingConfigurationDeletion(String Name, File ConfigurationFile) throws IOException {        
        if (JOptionPane.YES_OPTION==PopYesNoQuestion(Name, "Delete existing configuration in:\n\n"+ ConfigurationFile.getCanonicalPath())) {
            NetworkManager.RecursiveDelete(ConfigurationFile);            
        }        
    }   //CheckForExistingConfigurationDeletion
    
    public static void DisplayMessageContent(String Name, Message TheMessage) {        
        try {           
            String ToDisplay = "--- Message Start ---\n";
            ElementIterator MyElementIterator = TheMessage.getMessageElements();          
            while (MyElementIterator.hasNext()) {                
                MessageElement MyMessageElement = MyElementIterator.next();                
                ToDisplay = ToDisplay + "Element : " +
                        MyElementIterator.getNamespace() + " :: "
                        + MyMessageElement.getElementName() 
                        + "  [" + MyMessageElement + "]\n";                
            }            
            ToDisplay = ToDisplay + "--- Message End ---";            
            PopInformationMessage(Name,ToDisplay);
            
        } catch (Exception Ex) {            
            PopErrorMessage(Name, Ex.toString());            
        }        
    }   //DisplayMessageContent
    
    public static void GoToSleep(long Duration) {        
        long Delay = System.currentTimeMillis() + Duration;
        while (System.currentTimeMillis()<Delay) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException Ex) {
                // We don't care
            }
        } //while        
    } //GoToSleep

    /**
     *  Recursively copy elements beginnging with <code>from</code> into the
     *  document identified by <code>intoDoc</code>.
     *
     *  @param intoDoc  the document into which the elements which will be
     *  copied.
     *  @param intoElement  the element which will serve as the parent for
     *  the elements being copied.
     *  @param from the root element of the hierarchy which will be copied.
     *  @param copyAttributes whether the elements' attributes should be copied
     *         or not
     **/
    public static void copyElements(StructuredDocument intoDoc, Element intoElement, Element from, boolean recursive, boolean copyAttributes) {
        // Copying current level element
        Element newElement = intoDoc.createElement(from.getKey(), from.getValue());
        intoElement.appendChild(newElement);
        // Copy attributes (eventually)
        if (copyAttributes)  {
            if ((from instanceof Attributable) && (newElement instanceof Attributable)) {
                Enumeration eachAttrib = ((Attributable) from).getAttributes();
                while (eachAttrib.hasMoreElements()) {
                    Attribute anAttrib = (Attribute) eachAttrib.nextElement();
                    ((Attributable) newElement).addAttribute(anAttrib.getName(), anAttrib.getValue());
                }
            }
        } //if (copyAttributes) 
        // Looping through the child elements (eventually)
        if (recursive) {
            for (Enumeration eachChild = from.getChildren(); eachChild.hasMoreElements();) {
                // recurse to add the children.
                copyElements(intoDoc, newElement, (Element) eachChild.nextElement(), recursive, copyAttributes);
            }
        } //if (recursive)
    }
    
    public static void joinPeerGroup (PeerGroup grp) {
        StructuredDocument creds = null;
        try {           
            // Generate the credentials for the Peer Group
            AuthenticationCredential authCred = new AuthenticationCredential( grp, null, creds );          
            System.out.println("The Peer Group ID of this authCred is: "+ authCred.getPeerGroupID());            
            // Get the MembershipService from the peer group
            MembershipService membership = grp.getMembershipService();
            // Get the Authenticator from the Authentication creds
            net.jxta.membership.Authenticator auth = membership.apply(authCred);
            // All authenticators besides 'Null' require you to do something here.
            // Check if everything is okay to join the group
            if (auth.isReadyForJoin()){
                Credential myCred = membership.join(auth);
                System.out.println("Joined group " + grp.getPeerGroupName());
                // display the credential as a plain text document.
                System.out.println("\nCredential: ");
                StructuredTextDocument doc = (StructuredTextDocument)
                myCred.getDocument(new MimeMediaType("text/plain"));
                try (StringWriter out = new StringWriter()) {
                    doc.sendToWriter(out);
                    System.out.println(out.toString());
                }
            }   // if (auth.isReadyForJoin())
            else {
                System.out.println("Failure: unable to join group");
            }            
        } catch (Exception e){
            System.out.println("Failure in authentication");
        }
        
    } // joinPeerGroup
    
    public static byte[] longToBytes(long x) {
        return new byte[] {
            (byte)((x >> 56) & 0xff),
            (byte)((x >> 48) & 0xff),
            (byte)((x >> 40) & 0xff),
            (byte)((x >> 32) & 0xff),
            (byte)((x >> 24) & 0xff),
            (byte)((x >> 16) & 0xff),
            (byte)((x >> 8) & 0xff),
            (byte)((x >> 0) & 0xff),
        };
    }        
    public static long bytesToLong(byte[] data) {
        return (long) (
            (long)(0xff & data[0]) << 56|
            (long)(0xff & data[1]) << 48|
            (long)(0xff & data[2]) << 40|
            (long)(0xff & data[3]) << 32|
            (long)(0xff & data[4]) << 24|
            (long)(0xff & data[5]) << 16|
            (long)(0xff & data[6]) << 8|
            (long)(0xff & data[7]) << 0
        );
    }     
    public static byte[] intToByte(int data) {
        return new byte[] {
            (byte)((data >> 24) & 0xff),
            (byte)((data >> 16) & 0xff),
            (byte)((data >> 8) & 0xff),
            (byte)((data >> 0) & 0xff),
        };
    }    
    public static int byteToInt(byte[] data) {
        if (data == null || data.length != 4)
            return 0x0;
        return (int) (
                (0xff & data[0]) << 24 |
                (0xff & data[1]) << 16 |
                (0xff & data[2]) << 8 |
                (0xff & data[3]) << 0 
                );
    }    
    public static String fromJSON(String str){
        
        String result = str.replaceAll("[{\"}\\[\\],']","");
        result = result.replaceFirst("e:","");
        return result;
    }     
    public static String convertTime(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }
    public static String convertDate(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);
    }
    public static String getOS(){
        String os = System.getProperty("os.name");
        return os;        
    }
} 