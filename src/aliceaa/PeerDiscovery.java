/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package aliceaa;

import java.util.Enumeration;
import java.util.Vector;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JTextArea;
//import JXTA libraries
import net.jxta.discovery.DiscoveryEvent;
import net.jxta.discovery.DiscoveryListener;
import net.jxta.discovery.DiscoveryService;
import net.jxta.peergroup.PeerGroup;
import net.jxta.protocol.DiscoveryResponseMsg;
import net.jxta.protocol.PeerAdvertisement;


public class PeerDiscovery extends Thread implements Runnable,DiscoveryListener {
    
    private PeerGroup ontoPeerGroup = null;
    private DiscoveryService ontoDiscoService=null;

    public boolean endOfSearch = false;
    
    public PeerDiscovery(PeerGroup group) {
        this.ontoPeerGroup = group;
        ontoDiscoService = ontoPeerGroup.getDiscoveryService();
    }
    public void run() {   //this method will start this Thread
        while(true) {
            try{
            ontoDiscoService.getRemoteAdvertisements(null,DiscoveryService.PEER,null,null,10,this);
            Thread.sleep(5*1000);          
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }   // run
        
    @Override
    public void discoveryEvent(DiscoveryEvent de) {
        DiscoveryResponseMsg res = de.getResponse();
        String name = "unknown";
        PeerAdvertisement peerAdv = res.getPeerAdvertisement();

        if(peerAdv != null){
            name = peerAdv.getName();
        }
        
        PeerAdvertisement myAdv = null;
        Enumeration en = res.getAdvertisements();
        Vector peerList = new Vector();
        //Assigning new Peers to Vector and show them
        if(en != null){
            while(en.hasMoreElements()){
                myAdv = (PeerAdvertisement) en.nextElement();
                peerList.addElement(myAdv.getName());
                System.out.println("Found Peer:\n"+myAdv.getName()+"\nwith Adv\n"+res.getAdvertisements());
            }
        }   //if
    }   
}